var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var handleErrors = require('../util/handleErrors');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');

var config = require('../config').sass;

gulp.task('sass', function () {
    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(sass(config.settings).on('error', sass.logError))
        .on('error', handleErrors)
        .pipe(autoprefixer({browsers: ['last 2 version']}))
        .pipe(sourcemaps.write())
        .pipe(concat(config.bundle))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({stream: true}));
});
