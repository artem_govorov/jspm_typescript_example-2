/* Notes:
 - gulp/tasks/browserify.js handles js recompiling with watchify
 - gulp/tasks/browserSync.js watches and reloads compiled files
 */

var gulp = require('gulp');
var watch = require('gulp-watch');
var path = require('path');
var Builder = require('jspm').Builder;
var chalk = require('chalk');

var config = require('../config');

var builder = new Builder(config.baseUrl, path.join(config.baseUrl, 'config.js'));
builder.config({ sourceMaps: 'inline' });

gulp.task('watch', ['browserSync'], function (callback) {
    watch(config.sass.src, function () {
        gulp.start('sass');
    });
    watch(config.images.src, function () {
        gulp.start('images');
    });
    watch(config.templates.src, function () {
        gulp.start('templates');
    });

    watch(config.javascript.src, function (vinyl) {
        var buildStart = Date.now();
        // console.log('base:' + vinyl.base);
        var pathBits = vinyl.base.split('/');
        var dir = pathBits[pathBits.length - 1]; // get the last directory in the base dir.
        var filename = path.join(dir, vinyl.relative);
        builder.invalidate(filename);
        console.log('Invalidated', chalk.blue(filename), 'from cache');
        console.log('Starting build');
        builder.bundle('app', path.join('public/frontend', config.javascript.bundle), {sourceMaps: 'inline'})
            .then(function () {
                console.log('Build finished', chalk.red(Date.now() - buildStart));
            })
            .catch(function (err) {
                console.log('Build error');
                console.log(err.stack || err);
            })
    });

    watch(config.jspm_packages.src, function () {
        gulp.start('copy-jspm-libs');
    });
    watch(config.i18n.src, function () {
        gulp.start('i18n');
    });
});
