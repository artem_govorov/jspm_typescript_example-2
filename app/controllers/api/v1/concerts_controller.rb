module Api
  module V1

    ##
    # Controller syntax
    # Restrict Fields: ?fields=a,b,c
    # Sort: sort=name,-name
    # Include: embed=areas, devices
    class ConcertsController <  ::Api::ResourceController

      resource class: Concert

      index role: :any
      create role: :any

    end
  end

end