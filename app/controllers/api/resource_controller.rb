module Api

  class ResourceController < ApplicationController

    include ::ApiActions::Concerns::SerializableTransactions
    include ::ApiActions::ApiActionController

    # Provides the current_user method
    include Knock::Authenticable

    protect_from_forgery with: :null_session
    respond_to :json
    layout false

    # Make sure our modifying transactions are serializable.
    # Todo: if I remove from show/index then I'll have to update the API controller to not re-raise a rollback for show/index
    around_action :wrap_in_serializable_transaction #, except: [:show, :index]


    def authorized_user_for!(role)
      # user = current_user
      # raise ::ApiActions::Errors::NotAuthenticated unless user
      # raise ::ApiActions::Errors::NotAuthorized unless role == :any || user.authorized?(role)
      # # inject an updated token into the response. That way teh client will only expire after a period
      # # of inactiviity rather than a time after logging in.
      # response.headers['X-AUTH-TOKEN'] = Api::V1::Auth::AuthTokensController::CreateTokenAction.create_token_for(user).jwt
      # user
      nil
    end

    def publish(channel_id, envelope)
      ActionCable.server.broadcast(channel_id, envelope.as_json)
    end

    def params
      JsonUtils.dejsonize_params(super)
    end

  end

end