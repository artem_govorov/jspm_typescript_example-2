class Entity < ApplicationRecord

  self.abstract_class = true

  include Concerns::HasMutableAttributes

  def optimistic_lock_token
    self.updated_at
  end

end