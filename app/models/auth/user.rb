module Auth

  class User < Entity

    include Concerns::ClassyEnumArray

    has_create_attributes :username, :password, :password_confirmation
    has_mutable_attributes :first_name, :surname

    # acts_as_paranoid
    authenticates_with_sorcery!

    classy_enum_array :roles, class_name: 'Auth::Role'

    validates_presence_of :username
    validates_uniqueness_of :username

    # validates_strength_of :password, level: :good, if: ->(user) {user.password || user.new_record?}
    validates_presence_of :password, if: ->(user) {user.password || user.new_record?}
    validates_confirmation_of :password, if: :password


    def self.from_token_request request
      self.find_by! username: request[:username]
    end

    def self.from_token_payload payload
      # Returns a valid user, `nil` or raise
      self.find_by! id: payload["sub"]
    end

    # As per the knock requirements to behave like has_secure_password #authenticate method.
    def authenticate(password)
      self.valid_password?(password) ? self : false
    end

    def authorized?(role)
      roles.include?(role)
    end

    # we dont' need this checked during deletion, as if you're allowed to delete users, you can't delete yourself, hence
    # you can delete the last user manager.
    # def last_user_manager?
    #   roles.include?(Role::UserManagement) && User.where('roles LIKE :search', search: "%#{Role::UserManagement.new.to_s}%").count < 2
    # end


    def to_token_payload
      # Returns the payload as a hash
    end

  end

end