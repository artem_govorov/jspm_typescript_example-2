class BaseSerializer < ActiveModel::Serializer

  def self.serialize(resource, options = {})
    ApiActions::JsonFormat.transform_hash_keys(ActiveModelSerializers::SerializableResource.new(resource, {serializer: self, include: '**'}).as_json)
  end

end
