module Auth
  class UserProfileSerializer < EntitySerializer

    attributes :first_name, :surname
    has_many :roles

    def roles
      object.roles.map(&:to_s)
    end

  end
end