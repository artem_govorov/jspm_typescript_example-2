class EntitySerializer < TypedSerializer

  attributes :id, :optimistic_lock_token

end
