import {assert, lang, ConstructorFunction} from 'pectin/lang';
import angular from 'angular';
import {I18n} from 'app/i18n/i18n.class.js';
// noinspection TypeScriptCheckImport
import S from 'string';
//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import {PlaceDefinition} from "./placeDefinition.class";
import {AbstractTreeNode} from "./abstractTreeNode.class";
import {PlaceMetadata, VisitMetadata} from "./placeDecorators";
import {PlaceBuilder, DefaultPlaceBuilder} from "./placeBuilder.class";


export class Place extends AbstractTreeNode<Place> {

    static define(name:string):PlaceBuilder {
        return new DefaultPlaceBuilder(name);
    }

    private _definition:PlaceDefinition;
    private _parent:Place;
    
    constructor(definition:PlaceDefinition) {
        super();
        this._definition = assert.notNull(definition);
    }

    get definition():PlaceDefinition {
        return this._definition;
    }

    get name() {
        return this._definition.name;
    }

    get placeId() {
        let bits = [];
        if (this.parent && !this.parent.isRoot()) {
            bits.push(this.parent.placeId);
        }
        bits.push(this.name);
        return bits.join('.');
    }

    get dasherizedPlaceId() {
        return S(this.placeId).replaceAll('.', '-').s;
    }

    t(key) {
        return I18n.t(`ui.places.${this.placeId}.${key}`);
    }

    get title() {
        return this.t('title');
    }

    get iconName() {
        return this.t('nav_icon');
    }

    setParent(place:Place) {
        assert.notPresent(this.parent);
        this.parent = assert.notNull(place);
    }

    isActive():boolean {
        // return this._env.isActivePlace(this);
        return null;
    }

    add(child:Place) {
        this.children.push(child);
        child.setParent(this);
        this.notifyChildAdded(this, child);
    }

    notifyChildAdded(parent:Place, child:Place) {
        this.rootAncestor()['notifiedChildAdded'](parent, child);
    }

    // returnTo(visit) {
    //     lang.abstractMethod(this.constructor)();
    // }
    //
    // visit() {
    //     lang.abstractMethod(this.constructor)();
    // }
    //
    // activate(params = {}, options = {}) {
    //     return this._env.activate(this, params, options)
    // }

    requiresAuthentication() {
        const authType = Reflect.getMetadata(PlaceMetadata.AuthenticationType, this);
        if (!authType) {
            throw new Error(`Authentication type not specified, you need to decorate ${this.constructor.name} with either @Public or @Authenticated.`);
        }
        return authType != PlaceMetadata.Authenticated;
    }

}