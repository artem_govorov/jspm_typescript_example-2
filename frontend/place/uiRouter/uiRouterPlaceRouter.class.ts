//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';

import {assert, lang} from 'pectin/lang';


import {Place} from "../place.class";
import {AuthService} from "../authService.class";
import IWindowService = angular.ng.IWindowService;
import {UIRouter} from "angular-ui-router";
import {VisitMetadata, VisitHelper} from "../placeDecorators";
import {Pectin} from "../../pectin/pectin.class";
import {PlaceRouter} from "../placeRouter.class";
import {PlaceDefinition} from "../placeDefinition.class";
import {TransitionService} from "angular-ui-router";
import {Transition} from "angular-ui-router";
import {Places} from "../places.class";
import {after} from "../../pectin/binding/dsl";
import {Disposable} from "../../pectin/binding/disposable.class";


export class UiRouterPlaceRouter implements PlaceRouter {

    private uiRouter:UIRouter;
    private _currentVisit:any;
    private _garbage:Disposable;

    // private stateRegistry: StateRegistry;
    // private _windowService: IWindowService;
    // private _transitions:TransitionService;

    // constructor($stateProvider: StateProvider, $window: IWindowService, $transitions: TransitionService) {
    // constructor(uiRouter:UIRouter, authService:AuthService) {
    constructor(uiRouter:UIRouter) {
        this.uiRouter = assert.notNull(uiRouter);

        // this._windowService = assert.notNull($window);
        // this._transitions = assert.notNull($transitions);

        this.uiRouter.transitionService.onBefore({}, (transition: Transition) => {
            var state = transition.router.stateService;
            let place = <Place>transition.to()['place'];
            // if place requires authentication and no current user then redirect to login
            // return rootPlace.isAuthorized(place) ? true : transition.router.stateService.target(rootPlace.loginPlace().id);
            if (this._currentVisit) {
                Places.history().add(this._currentVisit);
            }
        });

        this.uiRouter.transitionService.onSuccess({}, (transition: Transition) => {
            var state = transition.router.stateService;
            let place = <Place>transition.to()['place'];
            this._garbage.dispose();
            this._currentVisit = transition.getResolveValue(place.definition.visitParameterName);
            this._garbage.registerDisposable(after.valueOf(VisitHelper.getRouteIdentifierModel(this._currentVisit)).changes((newValue, oldValue) => {
                if (oldValue != newValue) {
                    // console.log(`redirecting...`);
                    state.go(transition.to().name, {[place.definition.routeIdentifierPropertyName]: newValue}, {
                        location: !oldValue ? 'replace' : 'true',
                        notify:false,
                        reload:false
                    })
                }
            }));
            VisitHelper.invokeInitMethod(this._currentVisit);
            // FocusUtil.applyAutoFocus($document.find('body'));
        });
    }

    installRoutes(place: Place): void {
        console.log(`adding -> ${place.placeId}`);
        const def = place.definition;
        this.uiRouter.stateRegistry.register({
            name: place.placeId,
            url: this.urlFor(place),
            place: place, // required for authentication hooks...
            templateProvider: def.templateProvider,
            controller: def.componentClass,
            controllerAs: def.componentAs,
            resolve: {
                [def.visitParameterName]: ($injector:angular.auto.IInjectorService) => $injector.instantiate(def.visitClass)
            }
        });
    }

    urlFor(place: Place) {
        const def = place.definition;
        var routeIdentifier = def.routeIdentifierPropertyName;
        return routeIdentifier ? `/${def.name}/{${routeIdentifier}}` : `/${def.name}`
    }
}