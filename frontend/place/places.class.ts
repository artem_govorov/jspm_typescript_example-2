import {assert} from "../pectin/lang";
//noinspection TypeScriptCheckImport
import memoize from 'memoize-decorator';

import {PlaceRouter} from "./placeRouter.class";
import {Place} from "./place.class";
import {PlaceDefinition} from "./placeDefinition.class";
import {VisitHistory} from "./visitHistory.class";

class RootPlace extends Place {

    private _callback: (parent: Place, child: Place)=>void;

    constructor() {
        super(new PlaceDefinition(''));
    }

    rootAncestor() {
        return this;
    }

    get name() {
        return '';
    }

    notifyChildAdded(parent, child) {
        if (this._callback) {
            this._callback(parent, child)
        }
    }

    onChildAdded(callback: (parent: Place, child: Place)=>void) {
        this._callback = callback;
    }
}


export class Places {

    private static _router: PlaceRouter;
    private static _rootPlace: RootPlace;
    private static _history: VisitHistory;

    static setRouter(router: PlaceRouter) {
        console.log(`--> Places.setRouter`);
        assert.notPresent(this._router, 'Router as already been set');
        this._router = assert.notNull(router);
        this.rootPlace().visitChildren(child => {
            this._router.installRoutes(child);
        });
        console.log(`<-- `);
    }

    static add(place:Place) {
        console.log(`--> Places.add(${place.placeId})`);
        this.rootPlace().add(place);
        console.log(`<-- `);
    }

    private static rootPlace(): Place {
        console.log(`--> ${this.name}.rootPlace`);
        if (!Places._rootPlace) {
            Places._rootPlace = new RootPlace();
            Places._rootPlace.onChildAdded((parent, child) => {
                console.log(`--> RootPlace.onChildAdded`);
                // if we have a router and the child has a path to the root place
                // then install the routes
                if (Places._router && child.rootAncestor() == Places._rootPlace) {
                    Places._router.installRoutes(child);
                }
                console.log(`<-- `);
            });
        }
        console.log(`<-- `);
        return Places._rootPlace;
    }

    @memoize
    static history():VisitHistory {
        return new VisitHistory();
    }

}