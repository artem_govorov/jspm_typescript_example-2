Places & Visits
===============
 - Place is stateless and is concerned navigationaly hierarchy.
 - Places can be nested.
 - A single place can have multiple routes ('contacts', contacts/:id)
 - When the route is activated a visit is created.
 - Visits hold state relevant to the activated route.
 - A single visits can redirect and as such may across multiple routes (e.g. contacts -> contacts/{null} -> contacts/{default entity id})

Visit Lifecycle
---------------
Visits are created using the injector and initialized.
visit.initializeVisit:void|Promise<any>
```
Place.define('contacts')
    .visitType(ContactsVisit) // ctor ContactVisit(.., .., ..);
    .component(ContactsUi, tempalate); // ctor($contactsVisit, .., ..);
    .create();
```
```
@UiComponent({ })
class ContactsUi {
    constructor($contactsVisit:ContactsVisist) {
    
    }
}

@RouteIdentifier('contactId').asInteger() // can have one
@RouteParameter('age').asInteger() // can have many 
ContactsVisit {
  constructor(...) {
  }
  
  @OnInit
  ensureLoaded():any|Promise<any>
}
```

Building
```
PlaceService.setFactory(...)
```


