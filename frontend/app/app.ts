
import * as angular from "angular";
import {Places} from "../place/places.class";
import {Pectin} from 'pectin/pectin.class'
import pectinDirectives from 'pectin/directives.module'
import placeModule from 'place/place.module';

let application = angular.module('the-app', [
    placeModule.name,
    pectinDirectives.name
]);

export default application;