SystemJS.config({
    paths: {
        "github:": "jspm_packages/github/",
        "npm:": "jspm_packages/npm/",
        "bitbucket:": "jspm_packages/bitbucket/"
    },
    browserConfig: {
        "baseURL": "/frontend"
    },
    devConfig: {
        "map": {
            "clean-css": "npm:clean-css@3.4.10",
            "core-js": "npm:core-js@1.2.6",
            "child_process": "npm:jspm-nodelibs-child_process@0.2.0",
            "http": "github:jspm/nodelibs-http@0.2.0-alpha",
            "https": "npm:jspm-nodelibs-https@0.2.0",
            "url": "npm:jspm-nodelibs-url@0.2.0"
        },
        "packages": {
            "npm:amdefine@1.0.0": {
                "map": {
                    "fs": "npm:jspm-nodelibs-fs@0.2.0",
                    "module": "npm:jspm-nodelibs-module@0.2.0",
                    "path": "npm:jspm-nodelibs-path@0.2.0",
                    "process": "npm:jspm-nodelibs-process@0.2.0"
                }
            },
            "npm:clean-css@3.4.10": {
                "map": {
                    "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                    "commander": "npm:commander@2.8.1",
                    "fs": "npm:jspm-nodelibs-fs@0.2.0",
                    "http": "npm:jspm-nodelibs-http@0.2.0",
                    "https": "npm:jspm-nodelibs-https@0.2.0",
                    "os": "npm:jspm-nodelibs-os@0.2.0",
                    "path": "npm:jspm-nodelibs-path@0.2.0",
                    "process": "npm:jspm-nodelibs-process@0.2.0",
                    "source-map": "npm:source-map@0.4.4",
                    "url": "npm:jspm-nodelibs-url@0.2.0",
                    "util": "npm:jspm-nodelibs-util@0.2.0"
                }
            },
            "npm:commander@2.8.1": {
                "map": {
                    "child_process": "npm:jspm-nodelibs-child_process@0.2.0",
                    "events": "npm:jspm-nodelibs-events@0.2.0",
                    "fs": "npm:jspm-nodelibs-fs@0.2.0",
                    "graceful-readlink": "npm:graceful-readlink@1.0.1",
                    "path": "npm:jspm-nodelibs-path@0.2.0",
                    "process": "npm:jspm-nodelibs-process@0.2.0"
                }
            },
            "npm:core-js@1.2.6": {
                "map": {
                    "fs": "npm:jspm-nodelibs-fs@0.2.0",
                    "path": "npm:jspm-nodelibs-path@0.2.0",
                    "process": "npm:jspm-nodelibs-process@0.2.0",
                    "systemjs-json": "github:systemjs/plugin-json@0.1.2"
                }
            },
            "npm:graceful-readlink@1.0.1": {
                "map": {
                    "fs": "npm:jspm-nodelibs-fs@0.2.0"
                }
            },
            "npm:punycode@1.3.2": {
                "map": {
                    "process": "npm:jspm-nodelibs-process@0.2.0"
                }
            },
            "npm:source-map@0.4.4": {
                "map": {
                    "amdefine": "npm:amdefine@1.0.0",
                    "process": "npm:jspm-nodelibs-process@0.2.0"
                }
            },
            "npm:jspm-nodelibs-http@0.2.0": {
                "map": {
                    "http-browserify": "npm:stream-http@2.7.2"
                }
            },
            "npm:stream-http@2.7.2": {
                "map": {
                    "inherits": "npm:inherits@2.0.1",
                    "builtin-status-codes": "npm:builtin-status-codes@3.0.0",
                    "xtend": "npm:xtend@4.0.1",
                    "readable-stream": "npm:readable-stream@2.3.3",
                    "to-arraybuffer": "npm:to-arraybuffer@1.0.1"
                }
            },
            "npm:jspm-nodelibs-url@0.2.0": {
                "map": {
                    "url-browserify": "npm:url@0.11.0"
                }
            },
            "npm:url@0.11.0": {
                "map": {
                    "punycode": "npm:punycode@1.3.2",
                    "querystring": "npm:querystring@0.2.0"
                }
            },
            "github:jspm/nodelibs-http@0.2.0-alpha": {
                "map": {
                    "http-browserify": "npm:stream-http@2.7.2"
                }
            }
        }
    },
    transpiler: "ts",
    typescriptOptions: {
        "tsconfig": true
    },
    packages: {
        "app": {
            "defaultExtension": "ts",
            "main": "bootstrap.ts",
            "meta": {
                "*.ts": {
                    "loader": "ts"
                },
                "*.css": {
                    "loader": "css"
                },
                "*.html": {
                    "loader": "text"
                }
            }
        },
        "place": {
            "defaultExtension": "ts",
            "meta": {
                "*.ts": {
                    "loader": "ts"
                },
                "*.css": {
                    "loader": "css"
                },
                "*.html": {
                    "loader": "text"
                }
            }
        },
        "pectin": {
            "defaultExtension": "ts",
            "meta": {
                "*.ts": {
                    "loader": "ts"
                },
                "*.css": {
                    "loader": "css"
                },
                "*.html": {
                    "loader": "text"
                }
            }
        }
    }
});

SystemJS.config({
    packageConfigPaths: [
        "npm:@*/*.json",
        "npm:*.json",
        "github:*/*.json"
    ],
    map: {
        "angular": "npm:angular@1.5.8",
        "angular-animate": "github:angular/bower-angular-animate@1.5.8",
        "angular-cookies": "github:angular/bower-angular-cookies@1.5.8",
        "angular-hotkeys": "npm:angular-hotkeys@1.7.0",
        "angular-material": "github:angular/bower-material@1.1.1",
        "angular-ui-router": "npm:angular-ui-router@1.0.0-beta.3",
        "assert": "npm:jspm-nodelibs-assert@0.2.0",
        "autobind-decorator": "npm:autobind-decorator@1.3.3",
        "blakeembrey/pluralize": "github:blakeembrey/pluralize@1.2.1",
        "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
        "constants": "npm:jspm-nodelibs-constants@0.2.0",
        "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
        "css": "github:systemjs/plugin-css@0.1.29",
        "events": "npm:jspm-nodelibs-events@0.2.0",
        "fs": "npm:jspm-nodelibs-fs@0.2.0",
        "jwt-decode": "npm:jwt-decode@2.1.0",
        "lodash": "npm:lodash@4.6.1",
        "memoize-decorator": "npm:memoize-decorator@1.0.2",
        "module": "npm:jspm-nodelibs-module@0.2.0",
        "moment": "npm:moment@2.12.0",
        "net": "npm:jspm-nodelibs-net@0.2.1",
        "object-inspect": "npm:object-inspect@1.2.1",
        "os": "npm:jspm-nodelibs-os@0.2.0",
        "path": "npm:jspm-nodelibs-path@0.2.0",
        "process": "github:jspm/nodelibs-process@0.2.0-alpha",
        "reflect-metadata": "npm:reflect-metadata@0.1.8",
        "samsonjs/strftime": "github:samsonjs/strftime@0.9.2",
        "stream": "npm:jspm-nodelibs-stream@0.2.0",
        "string": "npm:string@3.3.1",
        "string_decoder": "npm:jspm-nodelibs-string_decoder@0.2.0",
        "substack/camelize": "github:substack/camelize@0.1.2",
        "text": "github:systemjs/plugin-text@0.0.7",
        "ts": "github:frankwallis/plugin-typescript@5.1.2",
        "util": "npm:jspm-nodelibs-util@0.2.0",
        "vm": "npm:jspm-nodelibs-vm@0.2.0"
    },
    packages: {
        "github:angular/bower-angular-animate@1.5.8": {
            "map": {
                "angular": "github:angular/bower-angular@1.5.8"
            }
        },
        "github:angular/bower-angular-aria@1.5.8": {
            "map": {
                "angular": "github:angular/bower-angular@1.5.8"
            }
        },
        "github:angular/bower-angular-cookies@1.5.8": {
            "map": {
                "angular": "github:angular/bower-angular@1.5.8"
            }
        },
        "github:angular/bower-material@1.1.1": {
            "map": {
                "angular": "github:angular/bower-angular@1.5.8",
                "angular-animate": "github:angular/bower-angular-animate@1.5.8",
                "angular-aria": "github:angular/bower-angular-aria@1.5.8",
                "css": "github:systemjs/plugin-css@0.1.29"
            }
        },
        "github:frankwallis/plugin-typescript@5.1.2": {
            "map": {
                "typescript": "npm:typescript@2.0.3"
            }
        },
        "npm:angular-hotkeys@1.7.0": {
            "map": {
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:angular-ui-router@1.0.0-beta.3": {
            "map": {
                "angular": "npm:angular@1.5.8",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:asn1.js@4.8.1": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "minimalistic-assert": "npm:minimalistic-assert@1.0.0",
                "vm": "npm:jspm-nodelibs-vm@0.2.0"
            }
        },
        "npm:bn.js@4.11.6": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0"
            }
        },
        "npm:browserify-aes@1.0.6": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "buffer-xor": "npm:buffer-xor@1.0.3",
                "cipher-base": "npm:cipher-base@1.0.3",
                "create-hash": "npm:create-hash@1.1.2",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "evp_bytestokey": "npm:evp_bytestokey@1.0.0",
                "fs": "npm:jspm-nodelibs-fs@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:browserify-cipher@1.0.0": {
            "map": {
                "browserify-aes": "npm:browserify-aes@1.0.6",
                "browserify-des": "npm:browserify-des@1.0.0",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "evp_bytestokey": "npm:evp_bytestokey@1.0.0"
            }
        },
        "npm:browserify-des@1.0.0": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "cipher-base": "npm:cipher-base@1.0.3",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "des.js": "npm:des.js@1.0.0",
                "inherits": "npm:inherits@2.0.1"
            }
        },
        "npm:browserify-rsa@4.0.1": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "constants": "npm:jspm-nodelibs-constants@0.2.0",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "randombytes": "npm:randombytes@2.0.3"
            }
        },
        "npm:browserify-sign@4.0.0": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "browserify-rsa": "npm:browserify-rsa@4.0.1",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hash": "npm:create-hash@1.1.2",
                "create-hmac": "npm:create-hmac@1.1.4",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "elliptic": "npm:elliptic@6.3.2",
                "inherits": "npm:inherits@2.0.1",
                "parse-asn1": "npm:parse-asn1@5.0.0",
                "stream": "npm:jspm-nodelibs-stream@0.2.0"
            }
        },
        "npm:buffer-xor@1.0.3": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:cipher-base@1.0.3": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "stream": "npm:jspm-nodelibs-stream@0.2.0",
                "string_decoder": "npm:jspm-nodelibs-string_decoder@0.2.0"
            }
        },
        "npm:core-util-is@1.0.2": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0"
            }
        },
        "npm:create-ecdh@4.0.0": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "elliptic": "npm:elliptic@6.3.2"
            }
        },
        "npm:create-hash@1.1.2": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "cipher-base": "npm:cipher-base@1.0.3",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "fs": "npm:jspm-nodelibs-fs@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "ripemd160": "npm:ripemd160@1.0.1",
                "sha.js": "npm:sha.js@2.4.5"
            }
        },
        "npm:create-hmac@1.1.4": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hash": "npm:create-hash@1.1.2",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "stream": "npm:jspm-nodelibs-stream@0.2.0"
            }
        },
        "npm:crypto-browserify@3.11.0": {
            "map": {
                "browserify-cipher": "npm:browserify-cipher@1.0.0",
                "browserify-sign": "npm:browserify-sign@4.0.0",
                "create-ecdh": "npm:create-ecdh@4.0.0",
                "create-hash": "npm:create-hash@1.1.2",
                "create-hmac": "npm:create-hmac@1.1.4",
                "diffie-hellman": "npm:diffie-hellman@5.0.2",
                "inherits": "npm:inherits@2.0.1",
                "pbkdf2": "npm:pbkdf2@3.0.8",
                "public-encrypt": "npm:public-encrypt@4.0.0",
                "randombytes": "npm:randombytes@2.0.3"
            }
        },
        "npm:des.js@1.0.0": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "minimalistic-assert": "npm:minimalistic-assert@1.0.0"
            }
        },
        "npm:diffie-hellman@5.0.2": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "miller-rabin": "npm:miller-rabin@4.0.0",
                "randombytes": "npm:randombytes@2.0.3",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:elliptic@6.3.2": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "brorand": "npm:brorand@1.0.6",
                "hash.js": "npm:hash.js@1.0.3",
                "inherits": "npm:inherits@2.0.1",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:evp_bytestokey@1.0.0": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hash": "npm:create-hash@1.1.2",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0"
            }
        },
        "npm:hash.js@1.0.3": {
            "map": {
                "inherits": "npm:inherits@2.0.1"
            }
        },
        "npm:inherits@2.0.1": {
            "map": {
                "util": "npm:jspm-nodelibs-util@0.2.0"
            }
        },
        "npm:jwt-decode@2.1.0": {
            "map": {
                "fs": "npm:jspm-nodelibs-fs@0.2.0",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:lodash@4.6.1": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:memoize-decorator@1.0.2": {
            "map": {
                "assert": "npm:jspm-nodelibs-assert@0.2.0"
            }
        },
        "npm:miller-rabin@4.0.0": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "brorand": "npm:brorand@1.0.6"
            }
        },
        "npm:moment@2.12.0": {
            "map": {
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:parse-asn1@5.0.0": {
            "map": {
                "asn1.js": "npm:asn1.js@4.8.1",
                "browserify-aes": "npm:browserify-aes@1.0.6",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hash": "npm:create-hash@1.1.2",
                "evp_bytestokey": "npm:evp_bytestokey@1.0.0",
                "pbkdf2": "npm:pbkdf2@3.0.8",
                "systemjs-json": "github:systemjs/plugin-json@0.1.2"
            }
        },
        "npm:pbkdf2@3.0.8": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hmac": "npm:create-hmac@1.1.4",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:public-encrypt@4.0.0": {
            "map": {
                "bn.js": "npm:bn.js@4.11.6",
                "browserify-rsa": "npm:browserify-rsa@4.0.1",
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "create-hash": "npm:create-hash@1.1.2",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "parse-asn1": "npm:parse-asn1@5.0.0",
                "randombytes": "npm:randombytes@2.0.3"
            }
        },
        "npm:randombytes@2.0.3": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:ripemd160@1.0.1": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:sha.js@2.4.5": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0",
                "fs": "npm:jspm-nodelibs-fs@0.2.0",
                "inherits": "npm:inherits@2.0.1",
                "process": "npm:jspm-nodelibs-process@0.2.0"
            }
        },
        "npm:string_decoder@0.10.31": {
            "map": {
                "buffer": "npm:jspm-nodelibs-buffer@0.2.0"
            }
        },
        "npm:typescript@2.0.3": {
            "map": {
                "crypto": "npm:jspm-nodelibs-crypto@0.2.0",
                "os": "npm:jspm-nodelibs-os@0.2.0",
                "source-map-support": "npm:source-map-support@0.5.0"
            }
        },
        "npm:jspm-nodelibs-crypto@0.2.0": {
            "map": {
                "crypto-browserify": "npm:crypto-browserify@3.11.0"
            }
        },
        "npm:jspm-nodelibs-string_decoder@0.2.0": {
            "map": {
                "string_decoder-browserify": "npm:string_decoder@0.10.31"
            }
        },
        "npm:jspm-nodelibs-os@0.2.0": {
            "map": {
                "os-browserify": "npm:os-browserify@0.2.1"
            }
        },
        "npm:jspm-nodelibs-stream@0.2.0": {
            "map": {
                "stream-browserify": "npm:stream-browserify@2.0.1"
            }
        },
        "npm:stream-browserify@2.0.1": {
            "map": {
                "inherits": "npm:inherits@2.0.1",
                "readable-stream": "npm:readable-stream@2.3.3"
            }
        },
        "npm:jspm-nodelibs-buffer@0.2.0": {
            "map": {
                "buffer-browserify": "npm:buffer@4.9.1"
            }
        },
        "npm:buffer@4.9.1": {
            "map": {
                "ieee754": "npm:ieee754@1.1.8",
                "isarray": "npm:isarray@1.0.0",
                "base64-js": "npm:base64-js@1.2.1"
            }
        },
        "npm:readable-stream@2.3.3": {
            "map": {
                "inherits": "npm:inherits@2.0.3",
                "string_decoder": "npm:string_decoder@1.0.3",
                "core-util-is": "npm:core-util-is@1.0.2",
                "isarray": "npm:isarray@1.0.0",
                "util-deprecate": "npm:util-deprecate@1.0.2",
                "process-nextick-args": "npm:process-nextick-args@1.0.7",
                "safe-buffer": "npm:safe-buffer@5.1.1"
            }
        },
        "npm:string_decoder@1.0.3": {
            "map": {
                "safe-buffer": "npm:safe-buffer@5.1.1"
            }
        },
        "npm:source-map-support@0.5.0": {
            "map": {
                "source-map": "npm:source-map@0.6.1"
            }
        }
    }
});
