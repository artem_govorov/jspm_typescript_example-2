import * as _ from 'lodash';

export type ConstructorFunction<T> = {
    new (...args:any[]):T;
    (): T;
    (...args:any[]):T;
    prototype:T
}


export var lang = {
    abstractMethod: function (constructor, name = null) {
        throw new Error(`${constructor.name} must implement abstract method ${name}`);
    },
    illegalState: function (message:string) {
        throw new Error('Illegal state: ' + message);
    },
    illegalArgument: function (message:string) {
        throw new Error('Illegal argument: ' + message);
    },
    notImplemented: function () {
        throw new Error('Method not implemented');
    },
    methodNotSupported: function (reason:string = null) {
        throw new Error('Method not supported' + reason ? ` ${reason}` : '');
    },
    "try": function (func) {
        return _.isFunction(func) ? func() : null;
    },
    wrapInArray: function (arrayOrOtherwise:any):Array {
        return _.isArray(arrayOrOtherwise) ? arrayOrOtherwise : [arrayOrOtherwise];
    },
    /**
     * Returns the prototype can of the object (including itself) with the first element
     * being the object itself.
     * @param object
     * @param uptoConstructor
     * @returns {any[]}
     */
    prototypeChain(object, uptoConstructor:any = Object) {
        assert.isTruthy(_.isObject(object));
        let prototypes = [];
        let proto = object;
        while (true) {
            proto = Object.getPrototypeOf(proto);
            if (!proto) break;
            prototypes.push(proto);
            if (proto.constructor == uptoConstructor) break;
        }
        return prototypes;
    }
};

export var assert = {
    notNull: <T> (value:T, name:string = 'value'):T => {
        if (!_.isString(value) && !value) throw new Error(`expected ${name} to be non null, got: ${typeof value}`);
        return value;
    },
    notNil: <T> (value:T, name:string = 'value'):T => {
        if (_.isNil(value)) throw new Error(`expected ${name} to be non null, got: ${typeof value}`);
        return value;
    },
    notPresent: function<T>(value:T, name:string = 'value'):void {
        if (value != null && typeof value !== 'undefined') throw new Error(`expected ${name} to be null or undefined, got: ${value}`);
    },
    notEmpty: function<T>(value:T[], name:string = 'value'):T[] {
        assert.notNull(value, name);
        assert.isInteger(value.length);
        if (value.length < 1) throw new Error(`expected ${name} have length > 0, got: ${value.length}`);
        return value;
    },
    notBlank: function(value:string, name:string = 'value'):string {
        assert.isString(value);
        assert.isInteger(value.length);
        if (value.length < 1) throw new Error(`expected ${name} have length > 0, got: ${value.length}`);
        return value;
    },
    isArray: <T>(array:T, name:string = 'value'):T => {
        if (!(array instanceof Array)) {
            throw new Error(`expected ${name} to be an array, got: ${typeof value}`);
        }
        return array;
    },
    isFunction: <T>(thing:T, message = null):T => {
        if (!(_.isFunction(thing))) {
            throw new Error(message || `expected to get a function, got: ${typeof value}`);
        }
        return thing;
    },
    isTruthy: <T>(value:T, message = null):T=> {
        if (!value) {
            throw new Error(message || `expected to get a truthy value, got: ${value}`);
        }
        return value
    },
    isFalsey: <T>(value:T, message = null):T=> {
        if (value) {
            throw new Error(message || `expected to get a falsey value, got: ${value}`);
        }
        return value
    },
    isInteger: <T>(value:T):T=> {
        if (!((typeof value === "number") && Math.floor(value) === value)) {
            throw new Error(`expected to get an integer, got a ${typeof value} with value ${value}`);
        }
        return value;
    },
    isNumber: <T>(value:T):T => {
        if (!(typeof value === "number")) {
            throw new Error(`expected to get a number, got a ${typeof value} with value ${value}`);
        }
        return value;
    },
    isString<T>(value:T):T {
        if (!(typeof value === "string")) {
            throw new Error(`expected to get a string, got a ${typeof value} with value ${value}`);
        }
        return value;
    },
    defined: (target:any, propertyName:string):void => {
        if (typeof target[propertyName] === 'undefined') throw new Error(`property ${propertyName} is undefined`);
    }
    // isSubclass(thing, abstractType) {
    //     assert.isFalsey(this.constructor.name == abstractType.name);
    // }
    //
};