export class Tuple<T> {
    left:T;
    right:T;
    constructor(left:T, right:T) {
        this.left = left;
        this.right = right;
    }
}