//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import {isEqualOrSame} from 'pectin/compare';
import {ValueModel} from "./baseValueModels";
import {ValueHolder} from "./baseValueModels";
import {lang} from "../lang";
import * as _ from 'lodash';
import {DebugContext} from "../debug/debugContext.class";
import {Trace} from "../debug/trace.class";

export interface IHasDirtyTracking {
    dirtyModel:ValueModel<boolean>;
    dirty:boolean;
    checkpoint():void;
    revert():void;
}



export function DirtyTracking(isEqual:(a:any, b:any)=>boolean = isEqualOrSame) {

    return function (targetClass) {

        let superDoWrite = targetClass.prototype.doWrite;
        let superDoRead = targetClass.prototype.doRead;
        let _superHandleSourceModelChange= targetClass.prototype._handleSourceModelChange;

        targetClass.prototype.doRead = function () {
            return superDoRead.call(this);
        };

        targetClass.prototype.doWrite = function (value) {
            superDoWrite.call(this, value);
            let changed = this.hasCheckpoint() && !isEqual(this['_checkpointValue'], value);
            // console.log(`hasCheckpoint=${this.hasCheckpoint()}`);
            // console.log(`checkpoint value=${this['_checkpointValue']}, ${typeof this['_checkpointValue']}`);
            // console.log(`current value=${value}, ${typeof value}`);
            // console.log(`_.isEqual=${_.isEqual(this['_checkpointValue'], value)}`);
            // console.log(`equalOrSame?=${isEqual(this['_checkpointValue'], value)}`);
            // console.log(`changed=${changed}`);
            this._setDirty(changed);
        };

        targetClass.prototype._handleSourceModelChange = function (newSource, oldSource) {
            _superHandleSourceModelChange.call(this, newSource, oldSource);
            this.checkpoint();
            this._setDirty(false);
        };


        //targetClass.prototype.commit = function () {
        //    this._setDirty(false);
        //    superDoWrite.call(this, this._cachedValue);
        //    this['_checkpointValue'] = this._cachedValue;
        //};

        //targetClass.prototype.setAutoCommit = function (autoCommit) {
        //    this['_autoCommit'] = autoCommit;
        //};
        //
        //targetClass.prototype.isAutoCommit = function () {
        //    return !!this['_autoCommit'];
        //};
        //

        targetClass.prototype.checkpoint = function () {
            this['_hasCheckpoint'] = true;
            const newValue = superDoRead.call(this);
            // Trace.log(`check pointing source value: ${inspect(newValue)}`);
            // Trace.log(`check pointing my value: ${inspect(this.value)}`);
            this['_checkpointValue'] = newValue;
            this._setDirty(false);
        };

        targetClass.prototype.revert = function () {
            // Trace.log(`reverting to: ${this['_checkpointValue']}`);
            this.value = this['_checkpointValue'];
            this.checkpoint();
        };

        targetClass.prototype.hasCheckpoint = function () {
            return this['_hasCheckpoint'];
        };

        Object.defineProperty(targetClass.prototype, '_internalDirtyModel', {
            get: function() {
                if (!this['_internalDirtyModel_Model']) {
                    this['_internalDirtyModel_Model'] = new ValueHolder<boolean>(false);
                    // DebugContext.inject(this['_internalDirtyModel_Model'], 'dirtyModel', this)
                }
                return this['_internalDirtyModel_Model'];
            }
        });

        Object.defineProperty(targetClass.prototype, 'dirtyModel', {
            get: function () {
                return this['_internalDirtyModel'].immutable();
            }
        });

        Object.defineProperty(targetClass.prototype, 'dirty', {
            get: function () {
                return this['_internalDirtyModel'].value;
            }
        });

        targetClass.prototype._setDirty = function (dirty) {
            this['_internalDirtyModel'].value = dirty;
        };

    }
}