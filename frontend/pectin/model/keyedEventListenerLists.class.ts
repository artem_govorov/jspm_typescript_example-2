// import * as _ from 'lodash';
import {EventListenerList} from "./eventListenerList.class";
import {IDisposable} from "../binding/disposable.class";
import {assert} from "../lang";

export class KeyedEventListenerLists<T> {

    private _keyedListeners = new Map<string, EventListenerList<T>>();


    subscribe(key:string, callback:T):IDisposable {
        assert.notNull(key);
        assert.isFunction(callback);
        let list = this.getListenerList(key);
        return list.add(callback);
    }

    fire(key:string, event:any) {
        const listenerList = this.getListenerList(key);
        listenerList.fire(listener => listener(event));
    }

    private getListenerList(key:String) {
        let list = this._keyedListeners.get(key);
        if (!list) {
            list = new EventListenerList();
            this._keyedListeners.set(key, list);
        }
        return list;
    }
}