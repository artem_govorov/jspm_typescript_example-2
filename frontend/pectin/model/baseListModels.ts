//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';

import * as _ from 'lodash';
import {lang, assert} from '../lang';
import {Mixin, mixinClass} from '../traits';
import {AbstractHasValue, HasValue} from './abstractHasValue.class';
import {HasSourceModels, HasSourceModelsTrait} from './hasSourceModels.trait';
import {DelegatingModel, IDelegatingListModel} from './delegatingModel.decorator';
import {Predicate, Transform} from './functions';
import {IDelegatingModel} from "./delegatingModel.decorator";
import {Disposable, IDisposable} from "../binding/disposable.class";
import {ValueModel} from "./baseValueModels";
import {traceIfDebugContext} from "../debug/debug.decorators";
import {ValueChangeTransaction} from "./hasValueChangeListeners.class";

export interface ListModel<T> extends HasValue<T[]> {
    value:T[];
    values:T[];
    length:number;
    includes(value:T):boolean;
    any():boolean;
    empty():boolean
    select(func:(value:T) => boolean):ListModel<T>;
    reject(func:(value:T) => boolean):ListModel<T>;
    find(predicate:Predicate<T>):T
    map<M>(func:(value:T) => M):ListModel<M>;
    immutable():ListModel<T>;
}

export interface MutableListModel<T> extends ListModel<T> {
    push(value:T);
    remove(value:T);
}

/**
 * Exists in the absence of the generic ValueModel<Array>.
 */
export class AbstractListModel<T> extends AbstractHasValue<T[]> implements ListModel<T> {

    private _immutableListModel:ListModel<T>;

    coerceValue(value:T | T[]) {
        return !value ? [] : (value instanceof Array) ? value : [value];
    }

    doWrite(value:T|T[]) {
        lang.illegalState('The ' + this.constructor.name + 'value model is readonly');
    }

    withDefault(value:T[]):ListModel<T> {
        var model = new DelegatingListModel<T>(this);
        model.setDefault(value);
        return model;
    };

    /**
     * Alias for #value that's reads better when you're dealing
     * with collections.
     */
    get values():T[] {
        return this.value
    }

    get length():number {
        return this.value ? this.value.length : 0;
    }

    includes(value:T):boolean {
        return _.includes(this.values, value);
    }

    any():boolean {
        return this.value.length > 0;
    }

    empty():boolean {
        return !this.any();
    }

    map<T,S>(func:Transform<T,S>):ListModel<S> {
        return new MappedListModel<T,S>(this, func);
    }

    reject(predicate:Predicate<T>):ListModel<T> {
        return new ComputedListModel<T,T>(this, (values:T[]) => _.reject(values, predicate));
    }

    select(predicate:Predicate<T>):ListModel<T> {
        return new ComputedListModel<T,T>(this, values => _.filter(values, predicate));
    }

    find(predicate:Predicate<T>):T {
        return _.find(this.values, predicate);
    }

    immutable():ListModel<T> {
        if (!this._immutableListModel) {
            this._immutableListModel = new ImmutableListModel(this);
        }
        return this._immutableListModel;
    }

    /**
     * Fires a change to the list by replacing array with a copy of the current one.  This is
     * useful if you've changed and element of the list and want dependents to update or re-render.
     */
    fireChange() {
        this.value = this.value.slice();
    }

}

// todo: make this a ListHolder that implements MutableListModel that extends ListModel
export class ListHolder<T> extends AbstractListModel<T> implements MutableListModel<T> {
    private _value:Array<T>;

    constructor(initialValue = []) {
        super();
        this.value = initialValue;
    }

    doWrite(value:T | T[]) {
        this._value = value;
    }

    doRead():T[] {
        return this._value;
    }

    push(value:T) {
        this.value = (this.value || []).concat([value]);
    }

    remove(value:T) {
        this.value = _.reject(this.values, v => v === value);
    }

    replace(oldValue:T, newValue:T) {
        let index = this.values.indexOf(oldValue);
        if (index >= 0) {
            let newList = [].concat(this.values);
            newList[index] = newValue;
            this.value = newList;
        }
    }
}


// temp hack till the IDE is ok with the syntax
const NEVER_COMPUTED = Object.freeze({});

@Mixin(HasSourceModelsTrait)
export abstract class AbstractComputedListModel<T> extends AbstractListModel<T> {

    private _neverComputed = true;
    private _computedValue: T[];

    constructor() {
        super();
    }

    doRead() {
        // compute on first call if not prior.
        // This creates double computes when firing change events as the read of the previous
        // value triggers a compute, as does the actual handling of the event.
        if (this.valueHasNeverBeenComputed()) {
            this.commitTransaction();
        }
        return this._computedValue;
    }

    abstract doComputeValue(): T[];

    /**
     * Recomputes the models value and fires change event.
     */
    recompute() {
        this.fireValueChange();
    }

    fireChange():void {
        this.recompute();
    }

    ////@traceIfDebugContext
    onSourceValueChanged(newValue:T, oldValue:T, source:ListModel<any>) {
        this.fireValueChangeAfter(() => {});
    }

    getValuePriorToChange(): T[] {
        return this._neverComputed ? null : this._computedValue;
    }

    protected commitTransaction() {
        this._neverComputed = false;
        this._computedValue = this.doComputeValue();
        return this.value;
    }

    valueHasNeverBeenComputed() {
        return this._neverComputed;
    }
}
export interface AbstractComputedListModel<T> {
    dependsOn(source:AbstractHasValue<T>|AbstractHasValue<T[]>):AbstractComputedListModel<T>
}
// todo, clean this up to use a regular delegator rather than a mixin
//// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface AbstractComputedListModel<T> extends HasSourceModels<T> {}

/**
 * Exists in the absence of the generic ValueModel<Array>.
 */
export class ComputedListModel<T, S> extends AbstractComputedListModel<T> {

    private _source: ValueModel<S> | ListModel<S>;
    compute:(source:S[]) => T[];

    constructor(source:ValueModel<S> | ListModel<S>, fn?:(source:S[]) => T[]) {
        super();
        this._source = assert.notNull(source, 'source');
        this['dependsOn'](source);
        if (fn) {
            this.compute = fn;
        }
    }

    get source() {
        return this._source;
    }

    doComputeValue():T[]{
        let values = this.source ? this.source.value : null;
        return values ? this.compute(values) : [];
    }

    compute(values:S|S[]):T[] {
        return lang.abstractMethod(this.constructor)();
    }

}


export class ImmutableListModel<T> extends ComputedListModel<T, T> {

    constructor(source:ListModel<T>) {
        super(source);
    }

    compute(values:T):T[] {
        return [].concat(values);
    }
}


@DelegatingModel
abstract class AbstractDelegatingListModel<T> extends AbstractComputedListModel<T> {
}
// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface AbstractDelegatingListModel<T> extends IDelegatingListModel<T> {}

export class DelegatingListModel<T> extends AbstractDelegatingListModel<T> {

    constructor(delegate:ListModel<T> = new ListHolder<T>()) {
        super();
        if (_.isObject(delegate) && !(delegate instanceof AbstractListModel)) {
            lang.illegalArgument('delegate must be a ListModel');
        }
        this.setDelegate(delegate);
    }

    // needed by AbstractComputedListModel transaction finalization.
    updateComputedValue() {
        // noop
    }

    push(value) {
        let push = assert.isFunction(this.getDelegate()['push']);
        push(value);
    }

    remove(value) {
        let remove = assert.isFunction(this.getDelegate()['remove']);
        remove(value);
    }
}


/**
 * Exists in the absence of the generic ValueModel<Array>.
 */
export class MappedListModel<T,S> extends ComputedListModel<T,S> {

    mapValue:(value:S) => T;

    constructor(sourceModel:ListModel<S>, mapFunction?:(value:S)=>T) {
        super(sourceModel);
        if (mapFunction) {
            this.mapValue = mapFunction;
        }
    }

    mapValue(value:S):T {
        return lang.abstractMethod(this.constructor)();
    }

    compute(values:S[]):T[] {
        this.beforeMap();
        return _.map(values, value => this.mapValue(value));
    }

    beforeMap() {
        // hook for subclasses to do any initialisation work.
    }

}

/**
 * Exists in the absence of the generic ValueModel<Array>.
 */
export class SortedListModel<T> extends ComputedListModel<T,T> {

    doSort:(values:T[]) => T[];

    constructor(sourceModel:ListModel<T>, sortFunction?:(values:T[])=>T[]) {
        super(sourceModel);
        if (sortFunction) {
            this.doSort = sortFunction;
        }
    }

    doSort(values:T[]):T[] {
        return lang.abstractMethod(this.constructor)();
    }

    compute(values:T[]) {
        return this.doSort(values)
    }

}

export abstract class ConvertingListModel<T,S> extends ComputedListModel<T,S> implements MutableListModel<T> {

    constructor(source:ListModel<T>) {
        super(source);
    }

    doWrite(value:T) {
        this._ignoringSourceValueChangeEvents(() => {
            this.source.value = this.toSource(value);
        });
    }

    compute(value) {
        return this.fromSource(value)
    }

    fromSource(values:S[]):T[] {
        return lang.abstractMethod(this.constructor)();
    }

    toSource(values:T[]):S[] {
        return lang.abstractMethod(this.constructor)();
    }

}

