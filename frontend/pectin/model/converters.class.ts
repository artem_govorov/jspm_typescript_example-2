import {Converter} from "./converter.class";

class IdentityConverter<T> implements Converter<T,T> {

    fromSource(sourceValue:T):T {
        return sourceValue;
    }

    toSource(convertedValue:T):T {
        return convertedValue;
    }
}

export class Converters {
    static identity<T>():Converter<T,T> {
        return new IdentityConverter<T>();
    }

    static multiplyBy<number>(amount):Converter<number, number> {
        return {
            fromSource: (num) => num * amount,
            toSource: (num) => num / amount
        }
    }
}
