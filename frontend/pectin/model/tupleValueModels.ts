import {assert, lang} from 'pectin/lang';
import * as _ from 'lodash';
import {ValueModel, DelegatingValueModel, ValueHolder, ConvertingValueModel} from 'pectin/models';
import {Tuple} from "./tuple.class";
import {Pectin} from "../pectin.class";
import {IDelegatingModel, DelegatingModel} from "./delegatingModel.decorator";
import {HasSourceModelsTrait} from "./hasSourceModels.trait";
import {Mixin} from "../traits";

class LeafValueModel<T> extends ConvertingValueModel<T, Tuple<T>> {

    private source:TupleValueModel<T>;
    private isLeft;
    private factory:(left:T, right:T) => Tuple<T>;

    constructor(src:TupleValueModel<T>, isLeft:boolean) {
        super(src);
        this.isLeft = isLeft;
    }

    fromSource(value) {
        return value ? value[this.isLeft ? 'left' : 'right'] : null;
    }

    toSource(value) {
        let left = this.isLeft ? value : this.source.value.left;
        let right = this.isLeft ? this.source.value.right : value;
        return this._createTuple(left, right);
    }

    _createTuple(left, right) {
        return this.source._createTuple(left, right);
    }
}

export class TupleValueModel<T> extends ValueModel<Tuple<T>> {

    private _tupleType:(left:T, right:T)=> Tuple<T>;
    private _leftModel:ValueModel<T>;
    private _rightModel:ValueModel<T>;

    constructor(tupleType = null) {
        this._tupleType = tupleType || Pectin.get().defaultTupleType;
        this._leftModel = new LeafValueModel(this, true);
        this._rightModel = new LeafValueModel(this, false);
    }

    // is this correct, or should I use something like typeof Tuple?? But all I care about is the parameters..
    setTupleType(ctor:(left:T, right:T) => Tuple<T>) {
        this._tupleType = ctor;
    }

    left():ValueModel<T> {
        return this._leftModel
    }

    right():ValueModel<T> {
        return this._rightModel;
    }

    _createTuple(left, right) {
        return new this._tupleType(left, right);
    }

    coerceValue(value) {
        return value && !(value instanceof this._tupleType) ? new this._tupleType(value.left, value.right) : value;
    }

}

export class TupleValueHolder<T> extends TupleValueModel<T> {

    private _value:T;

    constructor(initialValue?:Tuple<T>) {
        super();
        this.value = initialValue;
    }

    doRead():Tuple<T> {
        return this._coerceToTuple(this._value);
    }

    doWrite(value:Tuple<T>) {
        this._value = this._coerceToTuple(value);
    }

}

@DelegatingModel
@Mixin(HasSourceModelsTrait)
export class DelegatingTupleValueModel<T> extends TupleValueModel<Tuple<T>> {

    constructor(delegate?:TupleValueModel<T>) {
        super();
        // not really test for tuple value model directly as it's just a value model, and there
        // could be other delegates and isolates involved in the chain. To change this I'd have
        // to create isolate.tuple(..), which I'm not sure is the right solution just yet.
        if (_.isObject(delegate) && !(delegate instanceof ValueModel)) {
            lang.illegalArgument('delegate must be a TupleValueModel although ValueModels are accepted for now');
        }
        this.setDelegate(delegate || new TupleValueHolder<T>());
    }
}
// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface DelegatingTupleValueModel<T> extends IDelegatingModel<T> {
}

