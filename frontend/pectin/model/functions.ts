export interface Predicate<T> {
    (item:T):boolean
}

export interface Transform<T,R> {
    (item:T):R
}
