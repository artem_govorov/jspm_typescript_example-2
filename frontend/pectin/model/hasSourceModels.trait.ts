import * as _ from 'lodash';

import {lang} from '../lang';
import {AbstractHasValue} from './abstractHasValue.class';
import {ValueModel} from "./baseValueModels";
import {traceIfDebugContext} from "../debug/debug.decorators";
import {Trace} from "../debug/trace.class";
import {IDisposable} from "../binding/disposable.class";

export interface HasSourceModels<T> extends IDisposable {
    sourceModels():ValueModel<T>;
    dependsOn(source:ValueModel<T>):void
    alreadyDependsOn(source:ValueModel<T>):boolean
    _ignoringSourceValueChangeEvents(func:()=>void):void
}

export var HasSourceModelsTrait = {

    sourceModels() {
        return Array.from(this._getSourceMap().keys());
    },

    _getSourceMap() {
        if (!this._sourceMap) {
            this._sourceMap = new Map();
        }
        return this._sourceMap;
    },

    dependsOn(sourceModel) {
        this._trackSource(sourceModel);
        return this;
    },

    alreadyDependsOn(sourceModel) {
        return !!this._getSourceMap().get(sourceModel);
    },

    dispose() {
        this._getSourceMap().forEach(registration => {
            registration.dispose();
        });
        this._getSourceMap().clear();
    },

    _handleSourceValueChange(newValue, oldValue, source) {
        if (!this.onSourceValueChanged) {
            lang.abstractMethod(this.constructor, 'onSourceValueChanged')();
        }
        this.onSourceValueChanged(newValue, oldValue, source);
    },

    _unTrackSource(source) {
        // delete returns true rather than the deleted value, so doing the get and delete separately
        let registration = this._getSourceMap().get(source);
        if (registration) registration.dispose();
        this._getSourceMap().delete(source);
    },

    _trackSource(source) {
        if (source) {
            if (!(source instanceof AbstractHasValue)) {
                lang.illegalState(`source must be an instance of AbstractModel, got ${source}`);
            }
            if (this._getSourceMap().has(source)) {
                lang.illegalState(`Source model already being tracked.`);
            }
            let registration = source.onValueChange((newValue, oldValue, source) => this._handleSourceValueChange(newValue, oldValue, source));
            this._getSourceMap().set(source, registration);
            return registration;
        }
        return null;
    }
};

