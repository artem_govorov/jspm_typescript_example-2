//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import * as _ from 'lodash';
import {lang} from '../lang';
import {IDisposable} from "../binding/disposable.class";
import {traceIfDebugContext, trace} from "../debug/debug.decorators";
import {
    ChangeListener, BeforeChangeListener, HasValueChangeListeners,
    AfterChangeListener
} from "./hasValueChangeListeners.class.ts";
import {DebugContext} from "../debug/debugContext.class";


export interface HasValue<T> {
    readonly value:T;
    onValueChange:(listener:ChangeListener<T>)=>IDisposable;
    onBeforeValueChange:(listener:BeforeChangeListener<T>)=>IDisposable;
    onAfterValueChange:(listener:AfterChangeListener<T>)=>IDisposable;
}

export class AbstractHasValue<T> extends HasValueChangeListeners<T> implements HasValue<T> {

    get value():T {
        return this.coerceValue(this.doRead());
    }

    set value(value:T) {
        if (this.inChangeTransaction()) {
            lang.illegalState('already in change transaction');
        }
        this.fireValueChangeAfter(() => {
            this.doWrite(this.coerceValue(value));
        });
    }

    doRead():T {
        return lang.abstractMethod(this.constructor);
    }

    doWrite(value:T) {
        lang.illegalState('The ' + this.constructor.name + 'is readonly');
    }

    coerceValue(value:any):T {
        return value;
    }

   //  @trace
    fireValueChangeAfter(func) {
        let tx = this.getTransaction();
        try {
            func();
        } finally {
            tx.commit();
        }
    }

    fireValueChange() {
        this.fireValueChangeAfter(()=>{})
    }

    commitTransaction():T {
        // needed because HasValueListeners doens't have a value getter.  Should really
        // fix that.
        return this.value;
    }

    getValuePriorToChange(): T {
        return this.value;
    }

    toString() {
        return `${DebugContext.get(this)}[value=${DebugContext.inspect(this.value)}]`
    }

}