import {assert, lang} from '../lang';
import {ValueModel} from './baseValueModels';
import {AbstractHasValue} from "./abstractHasValue.class";

export class PropertyValueModel<T> extends ValueModel<T>  {

    private propertyName:string;
    private target:any;

    constructor(propertyName:string) {
        super();
        this.propertyName = assert.notNull(propertyName);
    }

    doRead():T {
        return this.target ? this.target[this.propertyName] : undefined;
    }

    doWrite(value:T) {
        if (!this.target) {
            lang.illegalState("Can't write to property when target is null or undefined");
        }
        this.target[this.propertyName] = value;
    }

    bindTo(target:any) {
        this.target = target;
        return this;
    }

}

