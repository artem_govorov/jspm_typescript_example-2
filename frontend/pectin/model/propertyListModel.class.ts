import _ from 'lodash';
import {assert, lang} from '../lang';
import {AbstractListModel} from "./baseListModels";
import {MutableListModel} from "./baseListModels";

export class PropertyListModel<T> extends AbstractListModel<T> implements MutableListModel<T> {

    private propertyName:string;
    private target:any;

    constructor(propertyName:string) {
        super();
        this.propertyName = assert.notNull(propertyName);
    }

    doRead():T[] {
        return this.target ? this.target[this.propertyName] : undefined;
    }

    doWrite(value:T|T[]) {
        if (!this.target) {
            lang.illegalState("Can't write to property when target is null or undefined");
        }
        this.target[this.propertyName] = value;
    }

    push(value:T) {
        this.value = this.value.concat([value]);
    }

    remove(value:T) {
        this.value = _.reject(this.values, v => v === value);
    }


    bindTo(target) {
        this.target = target;
        return this;
    }

}

