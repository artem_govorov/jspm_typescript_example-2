import * as _ from 'lodash';
import {Decorator} from "./model/decorator.class";
import {Trace} from "./debug/trace.class";
import {DebugContext} from "./debug/debugContext.class";

export function isSame(a:any, b:any):boolean {
    if (!a && !b) return true;
    if ((!a && b) || (a && !b)) return false;
    a = Decorator.undecorate(a);
    b = Decorator.undecorate(b);
    return (a.constructor === b.constructor) && typeof a.isSame === 'function' && a.isSame(b);
}

export function isEqualOrSame(a:any, b:any):boolean {
    a = Decorator.undecorate(a);
    b = Decorator.undecorate(b);
    return _.eq(a, b) || isSame(a, b);
}

