
export function FormModelDirective($compile) {
    return {
        restrict: 'A',
        terminal: true, //this setting is important, stops angular processing children automatically.
        priority: 1000, //make sure we run before any other directives.
        compile: function compile(element, attrs) {

            element.removeAttr("form-model"); //remove ourselves so we don't recurse when compiling.
            element.attr('ng-model', `${attrs.valueModel}.value`);
            element.attr('ng-disabled', `!${attrs.valueModel}.enabled`);
            //element.attr('ng-required', `${attrs.formModel}.required !== undefined && ${attrs.formModel}.required`);

            let transclude = $compile(element);

            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    transclude(scope, function (clone, scope) {
                        iElement.replaceWith(clone);
                    });
                }
            };
        }
    }
}