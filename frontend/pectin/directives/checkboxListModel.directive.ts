import _ from 'lodash';

import pectinDirectives from '..//directives.module';

import {valueOf} from '..//binding/dsl';
import {Isolate} from '..//binding/isolate.class';
import {Command} from '..//commands';
import {assert} from '../lang';


class CheckboxListModelController {

    constructor($scope) {

        this.scope = $scope;

        //I18n.injectScope(this, "ui.component.form.date_picker");
        //
        //this.dateModel = Isolate.scope($scope).value(this.valueModel());
        //
        //this.selectCommand = this.getSelectCommand() || new DefaultSelectCommand(this.dateModel);
        //this.backCommand = this.getBackCommand() || new AdvanceCommand(this.dateModel, -1, this.selectCommand);
        //this.forwardCommand = this.getForwardCommand() || new AdvanceCommand(this.dateModel, 1, this.selectCommand);
    }

    get listModel() {
        return this.scope.listModelAccessor(this.scope);
    }

    select(value) {
        if (this.listModel.includes(value)) {
            this.listModel.remove(value);
        } else {
            this.listModel.push(value);
        }
    }

    includes(value) {
        return _.includes(this.listModel.values, value);
    }

}

let controllerName = '$checkboxListModelController';

export function CheckboxListModelDirective($parse, $compile) {
    return {
        restrict: 'A',
        priority: 1000,
        terminal: true,
        scope: true,
        controller: CheckboxListModelController,
        controllerAs: controllerName,
        compile: function compile(element, attrs) {

            element.removeAttr("checkbox-list-model"); //remove ourselves so we don't recurse when compiling.
            element.removeAttr("value"); // an our other attribute.

            let modelGetter = $parse(attrs.checkboxListModel);
            let valueName = attrs.value;

            element.attr('ng-click', `${controllerName}.select(${valueName})`);
            element.attr('ng-checked', `${controllerName}.includes(${valueName})`);

            let transclude = $compile(element);

            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                },
                post: function postLink(scope, iElement, iAttrs, controller) {
                    scope.listModelAccessor = modelGetter;
                    transclude(scope, function(clone, scope) {
                        iElement.replaceWith(clone);
                    });
                }
            };
        }
    }
};



