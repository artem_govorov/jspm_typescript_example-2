import {lang, assert} from '../lang';

import {Disposable, IDisposable} from './disposable.class';
import {ValueModel, ValueHolder, DelegatingValueModel, AbstractListModel, ListModel, DelegatingListModel} from '../models'
import {ListHolder} from "../model/baseListModels";
import {PropertyAdapter} from "./propertyAdapter.class";
import {TupleValueModel} from "../model/tupleValueModels";

let bindingAttrName = '__pectin_binding';

// TODO: Look into setting the delegate as the prototype so methods are passed on transparently where appropriate. See BaseDecorator.
export class Isolate implements IDisposable {

    static forScope(scope):Isolate {

        assert.notNull(scope);

        if (!scope[bindingAttrName]) {
            scope[bindingAttrName] = new Isolate();
            scope.$on('$destroy', () => scope[bindingAttrName].dispose());
        }

        return scope[bindingAttrName];
    }

    private _garbage:Disposable;

    constructor() {
        this._garbage = new Disposable();
    }

    dispose() {
        this._garbage.dispose();
    }

    track<T>(disposable:T):T {
        return this._garbage.registerDisposable(disposable);
    }

    // todo: if I add valueModel.createDelegate() then I can just have isolate.wrap(..) for any type that has that method
    value(objectOrModel, defaultValueOrModel = null) {

        if (!defaultValueOrModel) {
            assert.notNull(objectOrModel, 'objectOrModel');
        }

        if (!objectOrModel) {
            objectOrModel = defaultValueOrModel;
        }

        if (objectOrModel instanceof ValueModel) {
            var result = this.track(new DelegatingValueModel(objectOrModel));
            if (defaultValueOrModel) {
                result = result.withDefault(defaultValueOrModel);
            }
            return result;
        } else {
            return new ValueHolder(objectOrModel);
        }
    }
    
    list(arrayOrListModel, defaultValue = null) {

        if (!defaultValue) {
            assert.notNull(arrayOrListModel, 'arrayOrListModel');
        }

        if (!arrayOrListModel) {
            arrayOrListModel = defaultValue;
        }

        if (arrayOrListModel instanceof AbstractListModel) {
            return this.track(new DelegatingListModel(arrayOrListModel));
        }

        if (arrayOrListModel instanceof Array) {
            var model = new ListHolder();
            model.value = arrayOrListModel;
            return model;
        }

        lang.illegalState(`expected an instance of Array or ListModel, got: ${arrayOrListModel.constructor.name}`);
    }

    onChangeOf(source) {
        assert.notNull(source);
        let isolate = this;
        return {
            invoke(func) {
                assert.notNull(func);
                isolate.track(source.onValueChange(func));
            }
        }
    }

    bind(source) {
        assert.notNull(source);
        let isolate = this;
        return {
            toProperty: function (propertyName) {
                return {
                    on: function (target) {
                        isolate.track(new PropertyAdapter(source, target, propertyName));
                    }
                }
            }
        }
    }




}