import {ReducingValueModel} from 'pectin/models';

function and(model, other) {
    return new ReducingValueModel([model, other], (values) => values.reduce((a,b) => !!a && !!b));
}

function or(model, other) {
    return new ReducingValueModel([model, other], (values) => values.reduce((a,b) => !!a || !!b));
}

let mixin = {
    and: function(otherModel) {
        return and(this,otherModel);
    }
};

mixin.and.not = function(model) {
  valueOf(model)
};
export default mixin;