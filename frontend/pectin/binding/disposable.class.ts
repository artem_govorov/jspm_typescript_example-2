import * as _ from 'lodash';
import {assert,lang} from 'pectin/lang';
import {ValueModel} from "../model/baseValueModels";
import {ListModel} from "../model/baseListModels";

export interface IDisposable {
    dispose():void;
}

export interface GarbageCollector extends IDisposable {
    registerDisposable<T>(funcOrDisposable:T):T;
}

export class Disposable implements GarbageCollector {

    private _garbage:Array<any>;

    static removeFromList(list, item):IDisposable {
        return {
            dispose() {
                let index = list.indexOf(item);
                if (index > -1) {
                    list.splice(index, 1);
                }
            }
        }
    }

    static nullify(target, propertyName):IDisposable {
        return {
            dispose() {
                target[propertyName] = null;
            }
        }
    }

    constructor() {
        this._garbage = [];
    }

    onDispose(func:()=>void):void {
        this.registerDisposable(func);
    }

    onDisposeNullify(propertyName):void {
        this.onDispose(() => this[propertyName] = null);
    }

    registerDisposable<T>(funcOrDisposable:T):T {
        this._garbage.push(this._checkType(funcOrDisposable));
        return funcOrDisposable;
    }

    onChangeOf<T>(source:ValueModel<T>|ListModel<T>) {
        assert.notNull(source);
        let disposable = this;
        return {
            invoke(func) {
                assert.notNull(func);
                disposable.registerDisposable(source.onValueChange(func));
            }
        }
    }

    dispose():void {
        this._garbage.forEach(item => {
            this._doDispose(item)
        });
        // replace our garbage so any references it holds are also freed.
        this._garbage = [];
    }

    _doDispose(thing) {
        if (_.isFunction(thing)) {
            thing();
        } else {
            if (_.isFunction(thing.dispose)) {
                thing.dispose();
            }
        }
    }

    _checkType(thing):any {
        if (!_.isFunction(thing) && !_.isFunction(thing.dispose)) {
            lang.illegalState('`expected a disposable or function, got: ${thing.constructor.name}`')
        }
        return thing;
    }
}