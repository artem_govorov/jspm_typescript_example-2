source 'https://rubygems.org'

gem 'bundler', '>= 1.7.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '>= 5.0.0.rc2', '< 5.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5.x'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem 'responders', '~> 2.0'
gem 'active_model_serializers', github: 'rails-api/active_model_serializers', tag: 'v0.10.0.rc5'
gem 'transaction_isolation'
gem 'validates_timeliness', '~> 4.0'
# gem 'paranoia', '~> 2.0'
gem 'virtus'
gem 'knock', '~> 1.4.2'
gem 'sorcery', '~> 0.9.1'

# See config/i18n-js.yml for cofiguring where the files go
gem 'i18n-js', '3.0.0.rc10'

gem 'classy_enum'

gem 'factory_girl_rails'
gem 'faker', '~> 1.4'

gem 'ruby-prof', '~> 0.15'

gem 'rubytree' # used in our cucumber tests

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  gem 'cucumber'
  gem 'cucumber-rails', require: false
  gem 'require_all'
  gem 'site_prism', '~> 2.7'
  gem 'database_cleaner', '~> 1.6.1'
  gem 'selenium-webdriver'

  gem 'rb-readline' # See: http://stackoverflow.com/a/41386595/1248812

  gem 'rubocop', require: false
  gem 'clear_migrations'

  gem 'require_all'

  gem "rails-controller-testing", :git => "https://github.com/rails/rails-controller-testing"
  gem "rspec-rails", "~> 3.5.2"
  gem 'spreewald' # cucumber utilities (including timecop steps)
  gem 'rspec-activemodel-mocks'
  gem 'rspec-eventually'
  gem 'spring-commands-rspec'
  gem 'tapout', "~> 0.4"
  gem 'tapout', "~> 0.4"
  gem 'webmock', '~> 2.1.0'
  gem 'shoulda-matchers', '~> 2.8'
  gem 'launchy'
  gem 'chronic', "~> 0.10"
  gem 'chronic_duration', "~> 0.10"
  gem 'poltergeist', :git => 'https://github.com/teampoltergeist/poltergeist.git', :tag => 'v1.6.0'
  gem 'timecop'
  gem 'tod'
  gem 'rspec-activejob' #active job expectations: https://github.com/gocardless/rspec-activejob
  gem 'jsonpath', "~> 0.5"
  gem 'lol_dba', "~> 1.6"
  gem 'colorize'

end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
