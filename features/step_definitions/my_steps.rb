Given(/^I go to the concerts page$/) do
  @page = Page::ConcertsPage.new
  @page.load
end

And(/^I click the button$/) do
  @page.click_the_button
end