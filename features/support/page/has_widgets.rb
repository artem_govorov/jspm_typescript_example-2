module Page
  module HasWidgets

    def self.inject_widget(element, name, widget_module, *find_args)

      element.singleton_class.instance_exec do
        define_method name do |*runtime_args|
          capybara_element = find(*find_args, *runtime_args)
          # add the widget methods to the eigenclass of this instance only
          capybara_element.singleton_class.include widget_module
          capybara_element
        end
      end

      element.singleton_class.instance_exec do
        define_method "has_#{name}?" do
          all(*find_args).any?
        end

      end

      element
    end


    def widget(name, widget_module, *find_args)
      named = find_args.last.is_a?(Hash) ? find_args.last.delete(:as) : nil

      define_method name do |*runtime_args|
        capybara_element = find_first(*find_args, *runtime_args)
        # add the widget methods to the eigenclass of this instance only
        capybara_element.singleton_class.include widget_module
        capybara_element
      end

      define_method "has_#{name}?" do
        all(*find_args).any?
      end

      alias_method named, name if named
    end

  end
end