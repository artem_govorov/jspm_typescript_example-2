require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ConcertManager
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # todo configure this somehow!!!
    config.time_zone = 'Melbourne'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    I18n.available_locales = [:en]

    # config.autoload_paths << Rails.root.join('jobs') # why isn't this included by default???
    config.autoload_paths << Rails.root.join('lib')

    # config.active_job.queue_adapter = :que

    # # See http://blog.cuberoot.in/using-que-gem-with-phusion-passenger/
    # if defined?(PhusionPassenger)
    #   PhusionPassenger.on_event(:starting_worker_process) do |forked|
    #     if forked
    #       Que.mode = :async
    #     end
    #   end
    # end
  end
end
