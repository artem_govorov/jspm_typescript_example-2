module ApiActions
  module Concerns

    module SerializableTransactions

      extend ActiveSupport::Concern

      def wrap_in_serializable_transaction
        ActiveRecord::Base.isolation_level(:serializable) do
          ActiveRecord::Base.transaction do
            yield
          end
        end
      end

    end

  end
end