module ApiActions

  class ResourceConfig

    RESOURCE_KEYS = [:named, :class, :class_name, :serializer, :has_one, :attribute, :virtual, :identified_by]

    def self.create(options = {})

      options.symbolize_keys!
      options.assert_valid_keys(*RESOURCE_KEYS)
      virtual = options.delete(:virtual) || false
      resource_class = options.delete(:class) || options.delete(:class_name).constantize
      serializer = unless virtual
                     options.include?(:serializer) ? options.delete(:serializer) : "#{resource_class.name}Serializer".constantize
                   else
                     nil
                   end
      self.new.tap do |config|
        config.has_one = options.delete(:has_one)
        config.attribute = options.delete(:attribute)
        config.virtual = virtual
        config.publish = options.delete(:publish)
        config.identified_by = (options.include?(:identified_by) ? options.delete(:identified_by) : :id)
        config.resource_class = resource_class
        config.serializer_class = serializer
        config.resource_name = options.delete(:named).try(:to_s) || resource_class.name.demodulize.underscore
      end
    end

    attr_accessor :identified_by, :resource_class, :serializer_class, :resource_name
    attr_accessor :has_one, :attribute, :virtual, :publish

    [:has_one, :attribute, :virtual, :publish].each do |bool_attr|
      alias_method :"#{bool_attr}?", bool_attr
    end

  end

end