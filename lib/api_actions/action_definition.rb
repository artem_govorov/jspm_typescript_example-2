module ApiActions

  class ActionDefinition

    delegate :publish?, :collection?, to: :action_handler_class
    delegate :virtual?, to: :resource_definition

    attr_reader :resource_definition, :name, :action_handler_class, :role

    def initialize(resource_definition, name, action_handler_class, role)
      @resource_definition = resource_definition
      @name = Assert.present!(name)
      @action_handler_class = Assert.present!(action_handler_class)
      @role = Assert.present!(role)
    end

    def create_handler(controller)
      @action_handler_class.new(self, controller)
    end

    def inject_controller_method(controller)
      action_def = self
      controller.instance_exec do
        define_method(action_def.controller_method_name) do
          self.dispatch_request(action_def)
        end
      end

    end

    def inject_route(router_dsl)
        raise "http_method not specified for #{action_handler_class.name}" unless http_method
        raise "route_url not defined for #{action_handler_class.name}" unless route_url
        router_dsl.send(self.http_method, route_url, to: "#{self.controller_name}##{controller_method_name}")
    end

    def get_schema
      action_schema = {}
      action_schema[:method] = self.action_handler_class.get_http_method
      action_schema[:url] = self.route_url(parent_id_token: ':parent_id')
      action_schema[:raises] = JsonFormat.transform_values(exceptions) if exceptions.any?
      action_schema
    end

    def exceptions
      self.action_handler_class.exceptions_for(resource_definition)
    end

    def scopes
      self.action_handler_class.try(:scopes) || []
    end

    def http_method
      @action_handler_class.get_http_method
    end

    def route_url(parent_id_token: nil)
      @action_handler_class.route_url(self.resource_definition, parent_id_token: parent_id_token)
    end

    def controller_name
      resource_definition.controller.name.underscore[0..-12]
    end

    def controller_method_name
      resource_definition.controller_method_name_for(self)
    end

    def topic_name
      name
    end

    def member_action?
      action_handler_class.is_a?(ApiActions::Action::MemberAction)
    end

    # def create_message(resource, belongs_to: nil)
    #   # let the resource definition load the belongs to relationship if required, but all
    #   # the caller to specify it if it can't be automatically determined.
    #   self.resource_definition.topic(action_definition.name).create_message(payload: self.resource_definition.serialize(resource), belongs_to: belongs_to)
    # end

  end

end