module ApiActions
  module Errors
    class ResourceNotFound < Base
      def initialize(payload = {})
        super(410, payload)
      end
    end
  end
end