module ApiActions
  module Errors
    class InternalServerError < Base
      def initialize(message)
        super(500, {message: message})
      end
    end
  end
end