module ApiActions

  module ApiActionController

    extend ActiveSupport::Concern

    module ClassMethods

      def resource_definitions
        @resource_definition ? @resource_definition.flatten : []
      end

      def action_definitions
        self.resource_definitions.collect(&:action_definitions).flatten
      end

      def resource_definition
        @resource_definition ||= ApiActions::ResourceDefinition.new(self)
      end

      def topics
        resource_definitions.collect(&:topics).flatten
      end

      # Configures this controllers resource.
      def resource(options = {})
        self.resource_definition.builder.resource(options)
      end

      def action_definition(name)
        resource_definition.action_definition(name)
      end

      def topic(name)
        resource_definition.topic(name)
      end

      # Marks this controller as belonging to a parent resource.  When this is the case the controller
      # will associate the resource with r's parent scope on index, show and create.  Update and delete methods
      # are applied to the resource directly.
      def belongs_to(symbol, options = {})
        self.resource_definition.builder.belongs_to(symbol, options)
      end

      def define_collection_resource(name, options = {}, &block)
        self.resource_definition.define_nested_collection_resource(name, options, &block)
      end

      def define_has_one_resource(name, options = {}, &block)
        self.resource_definition.define_attribute_resource(name, options, &block)
      end

      def define_action(action_name, action_handler_class, role:)
        self.resource_definition.dsl.define_action(action_name, action_handler_class, role: role)
      end

      def define_topic(topic, topic_handler=nil)
        self.resource_definition.dsl.define_topic(topic, topic_handler)
      end

      def index(action_class=nil, role:)
        self.resource_definition.dsl.index(action_class || ApiActions::Action::Index, role: role)
      end

      def index_all(action_class=nil, role:)
        Lang.illegal_state!('index_all only allowed for belongs_to resources') unless resource_definition.belongs_to?
        root_resource = self.resource_definition.as_global_resource
        root_resource.dsl.index(action_class || ApiActions::Action::Index, role: role)
      end

      def show(action_class=nil, role:)
        self.resource_definition.dsl.show(action_class || ApiActions::Action::Show, role: role)
      end

      def create(action_class=nil, role:)
        self.resource_definition.dsl.create(action_class || ApiActions::Action::Create, role: role)
      end

      def update(action_class=nil, role:)
        self.resource_definition.dsl.update(action_class || ApiActions::Action::Update, role: role)
      end

      def destroy(action_class=nil, role:)
        self.resource_definition.dsl.destroy(action_class || ApiActions::Action::Destroy, role: role)
      end

      def on_raising_any(*symbol_or_exception, &block)
        self.resource_definition.dsl.on_raising_any(*symbol_or_exception, &block)
      end

    end

    included do
      delegate :resource_definition, :topics, to: :class
    end

    def authorized_user_for!(role)
      # subclasses can hook here to make sure they've been configured correctly.
    end

    def dispatch_request(action_def)
      # the action could be for a nested resource, so we use it's resource definition.
      response = Response.new(action_def.resource_definition)
      begin
        user = authorized_user_for!(action_def.role)
        action_handler = action_def.create_handler(self)
        action_handler.execute(response, params, request.headers, user)
        render json: response.to_envelope, serializer: false, status: response.status
        ApiActions.publish(response.messages) if response.messages.any?

      rescue ApiActions::Errors::Base => e
        render json: {error: e.as_json, messages: e.messages.as_json}, status: e.http_response_code
        raise ActiveRecord::Rollback
      end
    end

  end

end