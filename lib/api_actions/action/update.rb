module ApiActions
  module Action

      class Update < ApiActions::Action::MutateBase

        http_method :put

        raises :validation_error

        def on_execute
          if update_resource(resource_params)
            publish_action_message(resource)
            respond_with(resource)
          else
            raise_validation_error!(resource.errors)
          end
        end

        def permitted_params
          super + resource_class.mutable_attributes.as_permit_params
        end

        def update_resource(params)
          resource.update(params)
        end

      end

  end
end