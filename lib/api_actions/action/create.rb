module ApiActions
  module Action

    class Create < ApiActions::Action::CollectionAction

      define_model_callbacks :save

      http_method :post

      publish(true)

      raises :validation_error

      def on_execute

        set_resource(build_resource(resource_params))

        # add to our parent collection if we're a nested resource, this will add the parent reference.
        add_to_parent(resource) if resource_definition.belongs_to?

        run_callbacks(:save) do
          if save_resource(resource)
            respond_with(resource, status: :created)
            publish_action_message(resource)
          else
            raise_validation_error!(resource.errors.as_json)
          end
        end

      end

      def build_resource(params)
        resource_class.new(params)
      end

      def resource
        @resource
      end

      def set_resource(resource)
        @resource = resource
      end

      def save_resource(resource)
        resource.save
      end

      def add_to_parent(resource)
        parent_resource.send(resource_definition.inverse_of) << resource
      end

      def permitted_params
        resource_class.create_attributes.as_permit_params
      end

    end

  end
end