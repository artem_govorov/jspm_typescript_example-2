module ApiActions
  module Topic

    class InvalidateAllTopic < ::ApiActions::Topic::Topic

      valid_options(:belongs_to)

      def initialize(name, resource_def)
        super(name, resource_def)
        Lang.illegal_state!("invalidated messages only supported on collections and #{resource_def.schema_key} isn't one") unless resource_def.collection?
        Lang.illegal_state!("invalidated messages not supported virtual collections and #{resource_def.schema_key} is one") if resource_def.virtual?
      end

      def payload_class
        nil
      end

      def serialize_payload(resource, options)
        resource
      end

      def broadcast_channel_ids_for(resource, options)
        parent_token_id = if resource_def.belongs_to? && !self.resource_def.has_global_collection?
                            parent = options[:belongs_to] || resource.send(resource_def.parent_accessor)
                            parent.send(self.resource_def.parent_identified_by)
                          end

        [self.channel_id(parent_id_token: parent_token_id)]
      end

      def channel_id(parent_id_token: nil)
        definition = self.resource_def.has_global_collection? ? self.resource_def.global_resource_definition : resource_def
        definition.collection_url(parent_id_token: parent_id_token)
      end

    end

  end
end