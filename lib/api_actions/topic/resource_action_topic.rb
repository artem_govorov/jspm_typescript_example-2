module ApiActions
  module Topic
    class ResourceActionTopic < ApiActions::Topic::Topic

      valid_options(:belongs_to)

      def serialize_payload(resource, options)
        resource_def.serialize(resource)
      end

      def broadcast_channel_ids_for(resource, options)
        [].tap do |ids|
          parent_token_id = if resource_def.belongs_to?
                              parent = options[:belongs_to] || resource.send(resource_def.parent_accessor)
                              parent.send(self.resource_def.parent_identified_by)
                            end
          ids << self.channel_id(parent_id_token: parent_token_id)

          if resource_def.has_global_collection?
            ids << resource_def.global_resource_definition.collection_url
          end
        end
      end

      def channel_id(parent_id_token: nil)
        resource_def.collection_url(parent_id_token: parent_id_token)
      end

      def payload_class
        resource_def.resource_class
      end

    end

  end
end