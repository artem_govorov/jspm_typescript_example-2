module ApiActions

  # include Logging::Loggable

  def self.setup(&initializer_block)

    Rails.application.config.after_initialize do |app|

      ApiActions._initialize(&initializer_block)

      ActiveSupport::Reloader.after_class_unload do
        # Run our initializer block again and use constantize so we get the
        # freshly loaded version.
        # We need to run this directly after unload as the routes file will be
        # reloaded before `to_prepare` is called.
        'ApiActions'.constantize._initialize(&initializer_block)
      end

    end
  end

  def self.publish(message_or_messages)
    messages = Array.wrap(message_or_messages)
    Rails.logger.info("broadcasting #{messages.count} messages")
    ActionCable.server.broadcast('api_actions', messages: messages)
  end

  def self._initialize(&initializer_block)
    @config = ApiActions::Config.new
    initializer_block.call(@config)
  end

  def self.install_routes(router_dsl)
    @config.validate!
    @config.inject_routes(router_dsl)
  end

  def self.generate_schema
    @config.generate_schema
  end

  def self.write_schema
    schema = self.generate_schema
    FileUtils::mkdir_p File.dirname(@config.schema_generation_file)
    File.open(@config.schema_generation_file, 'w') { |f| f.write(JSON.pretty_generate(schema)) }
  end

  def self.config
    @config
  end

  def resource_definition_for(schema_key)
    @config.resource_definition_for(schema_key)
  end

end