module Assert

  def self.present!(thing, message = nil)
    raise Exceptions::IllegalArgumentError.new(message) unless thing
    thing
  end

  def self.nil!(thing)
    raise Exceptions::IllegalArgumentError if thing
  end

  def self.value(value, in_list:)
    raise Exceptions::IllegalArgumentError unless in_list.include?(value)
  end

  def self.truthy!(value, message = nil)
    raise Exceptions::IllegalArgumentError.new(message) if !value
  end

  def self.falsey!(value)
    raise Exceptions::IllegalArgumentError if value
  end

  def is_a!(thing, type)
    raise Exceptions::IllegalArgumentError unless thing.class < type
  end

end